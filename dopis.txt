Dobrý den,
ke konci listopadu jsem pod Vaším vedením absolvoval školení na android.

konečně jsem se dostal k tomu abych si něco vyzkoušel.
Nebyl jsem si jist jak s adapterama - tak jsem si našel tutorial:
https://blog.csdn.net/qq_39040714/article/details/115319188

podle kterého jsem postupoval...
pak jsem nějaké části zakomentoval a rozšířil ho. Ale část mého rozšíření nefunguje, jak bych si představoval.

co to má dělat:
když na nějakou položku kliknu, tak se má nadpis přeškrtnout a vpravo ikonka změnit na čtvereček s fajfkou a při opětovném kliknutí zase odškrtnout a fajfka zmizet.
je to něco jako odškrtávač nákupního seznamu ;-)

co to dělá:
problem 1) musím kliknout víckrát než se to zaškrtne/odškrtne (skoro mi připadá, jako by lichá čísla tuknutí byla ignorována)
problem 2) k řádkům na které se klikne a měly by se zaškrtnout se ještě náhodně zaškrtne něco dalšího
problem 3) možná souvisí s 2 - když po nastartování aplikace na nic nekliknu a sescroluju dolu, tak už je tam něco zaškrtlého.

můžete se prosím mrknout na kod a poradit mi, co tam je špatně (kod nasdilim přes googledisk)?
Snažil jsem se problémy vyřešit, ale už nevím co zkusit.

děkuji kamil kolofík

ps.: je to celé v javě, jelikož v práci potřebuju upravovat aplikaci v jave, tak si nechci plést hlavu ještě kotlinem.