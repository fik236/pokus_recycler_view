package com.fiksoftware.recycler;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

//jestli to chapu spravne, tak kazdej view kterej ma smysl plnit datama (takze asi linear layout atd...) ma svuj specialni adapter
//a tenhle nas adapter WordListAdapter je jakoby prizpusobenej adapter (ale nerozumim tomu, co je v tech <> zavorkach
public class WordListAdapter extends RecyclerView.Adapter<WordListAdapter.WordViewHolder> {
    private final ArrayList<Task> taskList;//tu si ulozime data, ktery mame zobrazit (abysme se furt neodkazovali na MainActivity), tak si to sem vlastne v konstruktoru zkopirujem
    private LayoutInflater mInflater; //inflate znamena nafoknout - takze tim se asi mysli naplneni dat

    public WordListAdapter(Context context, ArrayList<Task> taskList) {
        //tato proc se jmenuje stejne, jako cela trida, takze je to konstruktor
        mInflater = LayoutInflater.from(context);
        this.taskList = taskList; //do promene v teto tride si ulozime data ktera jsme si sem poslali z ManiActivity
    }

    @Override
    public WordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //View mItemView = mInflater.inflate(R.layout.wordlist_item, parent, false);
        View mItemView = mInflater.inflate(R.layout.slozita_polozka, parent, false);
        return new WordViewHolder(mItemView);
        //return new WordViewHolder(mItemView, this);
    }

    @Override
    public void onBindViewHolder(WordViewHolder holder, int position) {
        Task currentTask = taskList.get(position);

        holder.polozkaNadpis.setText(currentTask.getText());
        holder.polozkaText.setText(currentTask.getText() + " dasi text");
        holder.polozkaZaskrtavatko.setChecked(currentTask.isCompleted());
        if (currentTask.isCompleted()) {
            holder.polozkaNadpis.setPaintFlags(holder.polozkaNadpis.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            holder.polozkaNadpis.setPaintFlags(holder.polozkaNadpis.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        }

    }

    @Override
    public int getItemCount() {
        return taskList.size();
    }


    class WordViewHolder extends RecyclerView.ViewHolder {
        //public final TextView wordItemView;

        public final TextView polozkaNadpis;
        public final TextView polozkaText;
        public final CheckBox polozkaZaskrtavatko;

        public WordViewHolder(View itemView) {
            super(itemView);
            //wordItemView = itemView.findViewById(R.id.word);

            polozkaNadpis = itemView.findViewById(R.id.tv_nadpis);
            polozkaText = itemView.findViewById(R.id.tv_text);
            polozkaZaskrtavatko = itemView.findViewById(R.id.cb_zaskrtavatko);

            itemView.setOnClickListener(new View.OnClickListener() {
                //tohle mi reaguje na mackani vseho krom checkboxu (ale i tomu to zmeni stav)
                //ale kdyz kliknu primo na checkbox, tak ten se zakrtne, ale text se nepreskrtne - proto dale delam kouzla primo s checkboxem
                @Override
                public void onClick(View view) {
                    Toast.makeText(view.getContext(), "item " + getAdapterPosition(),Toast.LENGTH_LONG).show();
                    boolean newValue = !(taskList.get(getAdapterPosition()).isCompleted());
                    taskList.get(getAdapterPosition()).setCompleted(newValue);
                    //predtim se puvodni hodnota cetla primo z viewu checkboxu - coz mi neprislo prilis vhodne- jak je to na predchazejicich dvou radcich je to vic logicke
                    //taskList.get(getAdapterPosition()).setCompleted(!polozkaZaskrtavatko.isChecked());
                    notifyItemChanged(getAdapterPosition());
                }
            });

            //po macknuti samostatneho checkboxu mi to nepreskrtavalo text, tak jsem tady jeste musel dodelat tenhle on click listener a v nem si precist v jakem stavu je checkbox a zapsat to do daneho tasku a adapterovi rict, ze se to zmenilo
            polozkaZaskrtavatko.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(view.getContext(), "check " + getAdapterPosition(),Toast.LENGTH_LONG).show();
                    taskList.get(getAdapterPosition()).setCompleted(polozkaZaskrtavatko.isChecked());
                    notifyItemChanged(getAdapterPosition());
                }
            });



        }
    }

/*
    class WordViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        //public final TextView wordItemView;

        public final TextView polozkaNadpis;
        public final TextView polozkaText;
        public final CheckBox polozkaZaskrtavatko;

        final WordListAdapter mAdapter;

        public WordViewHolder (View itemView, WordListAdapter adapter) {
            super(itemView);
            //wordItemView = itemView.findViewById(R.id.word);

            polozkaNadpis = itemView.findViewById(R.id.tv_nadpis);
            polozkaText = itemView.findViewById(R.id.tv_text);
            polozkaZaskrtavatko = itemView.findViewById(R.id.cb_zaskrtavatko);

            polozkaZaskrtavatko.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(view.getContext(), "zaskrtavatko " + getAdapterPosition(),Toast.LENGTH_LONG).show();
                    if (polozkaZaskrtavatko.isChecked()) {
                        taskList.get(getAdapterPosition()).setCompleted(true);
                    }else {
                        taskList.get(getAdapterPosition()).setCompleted(false);
                    }
                    notifyItemChanged(getAdapterPosition());
                }
            });

            this.mAdapter = adapter;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            //takhle mi to taky zaregistruje macknuti na text view i na checkbox
            //kdyz macknu na textView nebo na mezeru mezi nim a checkboxem, tak se preskrtne text a zaskrtne checkbox
            //kdyz macknu primo checkbox, tak se zaskrtne, ale uz se nepreskrtne text
            //tak jsem musel primo do konstruktoru WordViewHolder pridat pro checkbox onclicklistenera a uz to funguje - ale nelibi se mi, jak jednoho listenera mam tam a jednoho tady
            Toast.makeText(v.getContext(), "neco " + getAdapterPosition(),Toast.LENGTH_LONG).show();

            Task macknutyTask = taskList.get(getLayoutPosition());
            macknutyTask.setCompleted(!macknutyTask.isCompleted());

            taskList.set(getLayoutPosition(),macknutyTask);
            mAdapter.notifyItemChanged(getLayoutPosition());
        }
    }

 */
}


