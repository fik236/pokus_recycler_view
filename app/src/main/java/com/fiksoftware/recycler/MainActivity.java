package com.fiksoftware.recycler;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;

import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.LinkedList;

//vyrabeno podle navodu:
//https://blog.csdn.net/qq_39040714/article/details/115319188

public class MainActivity extends AppCompatActivity {

    private final ArrayList<Task> taskList = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private WordListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //zacinal jsem aplikaci z templatu basic activity - proto je tou pousivanej tento layout, kterej pak odkazuje na content_main; stacilo by nasledujici radek zakomentovat a pouzit rovnou ten content main
        setContentView(R.layout.activity_main);
        //setContentView(R.layout.content_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //put initial data into list - timhle si pripravime nejaka testovaci data
        for (int i = 0; i< 20; i++) {
            taskList.add(new Task("slovo " + i,false));
        }

        //Get a handle to the RecyclerView
        mRecyclerView = findViewById(R.id.recyclerview);
        //create an adapter and suppl the data to be displayed; zavolame konstruktor naseho upravenyho adapteru a predame mu data, ktery chceme zobrazit
        mAdapter = new WordListAdapter(this, taskList);
        //connect the adapter with the RecyclerView - nasemu recycler Viewu pripojime nas upravnej adapter
        mRecyclerView.setAdapter(mAdapter);
        //give the recyclerview a default layout manager - tomuhle zatim nerozumim
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        //tohle plovouci tlacitko sem dorazilo z templatu basic activity
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int wordListSize = taskList.size();
                //add s new word to the wordList
                taskList.add(new Task("+ slovo " + wordListSize, false));
                //notify the adapter, that the data has changed
                mRecyclerView.getAdapter().notifyItemInserted(wordListSize);
                //scroll to the bottom
                mRecyclerView.smoothScrollToPosition(wordListSize);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //tahle funkce sem dorazila z templatu basic activity
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //tahle funkce sem dorazila z templatu basic activity
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}